-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2020 at 08:26 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `full_name`, `address`, `phone`, `email`, `username`, `password`, `role_id`, `is_deleted`) VALUES
(1, 'Afkar Subhan', 'Tuban', '085607741123', 'afkar49@gmail.com', 'afkar', 'afkar123', 1, 0),
(2, 'AdamA', 'Malang', '085607745674', 'adam@gmail.com', 'adam', 'aaa', 1, 0),
(3, 'Haha', 'Bang', '012607745674', 'haa@gmail.com', 'tes1', 'tes12', 1, 0),
(4, 'AdamAth_', 'Jember', '08582828282', 'athallahrich@gmail.com', 'adam1', 'adam1', 1, 0),
(5, 'Mas Kar', 'Jauh', '0867576567', 'athallahrich1@gmail.com', 'maskar', '123456', 1, 0),
(6, 'Aku', 'Jauhhhh', '085214213', 'athallahrich@gmail.com', 'adamaaa', 'aaa', 1, 0),
(7, 'CEO Admin', '-', '-', 'admin@gmail.com', 'ceo', 'ceo', 2, 0),
(8, 'HRD Staff', '-', '123123123', 'hrd@gmail.com', 'hrd', 'hrd', 3, 0),
(9, 'Production Manager', '-', '-', 'pm@gmail.com', 'pm', 'pm', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `detail_order_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `payment_name` varchar(255) NOT NULL,
  `output` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`detail_order_id`, `order_id`, `username`, `product_name`, `price`, `payment_name`, `output`, `address`, `is_deleted`) VALUES
(1, 2, 'Afkar', 'Product 1', 150000, 'OVO', 2, 'Tuban', 1),
(2, 3, 'Afkar', 'Product 1', 150000, 'OVO', 2, 'Tuban', 0),
(3, 4, 'Afkar', 'Product 1', 150000, 'OVO', 2, 'Tuban', 0);

-- --------------------------------------------------------

--
-- Table structure for table `global_variable`
--

CREATE TABLE `global_variable` (
  `global_variable_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_variable`
--

INSERT INTO `global_variable` (`global_variable_id`, `type`, `value`, `description`, `is_deleted`) VALUES
(1, '-', 1, 'inprogress', 0),
(2, '-', 2, 'confirmation', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `output` int(11) NOT NULL,
  `order_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `account_id`, `product_id`, `payment_id`, `amount`, `output`, `order_date`, `status_id`, `is_deleted`) VALUES
(3, 1, 1, 1, 300000, 2, '2020-04-18 13:51:52', 1, 0),
(4, 1, 1, 1, 300000, 2, '2020-04-18 18:54:41', 1, 0),
(5, 4, 1, 1, 360000, 3, '2020-04-19 22:36:11', 1, 0),
(6, 4, 1, 1, 240000, 2, '2020-04-19 22:37:51', 1, 0),
(7, 4, 1, 1, 120000, 1, '2020-04-20 10:38:15', 1, 0),
(8, 4, 1, 1, 120000, 1, '2020-04-20 10:44:38', 1, 0),
(10, 4, 1, 1, 120000, 1, '2020-04-21 08:06:58', 1, 0),
(11, 4, 1, 1, 360000, 3, '2020-04-21 09:01:48', 1, 0),
(12, 4, 3, 3, 20000, 1, '2020-04-21 09:14:14', 1, 0),
(13, 4, 3, 1, 20000, 1, '2020-04-21 09:29:07', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `payment_name` varchar(50) NOT NULL,
  `member_number` varchar(50) NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `payment_name`, `member_number`, `member_name`, `is_deleted`) VALUES
(1, 'OVO', '9521', 'Adam', 0),
(3, 'Dana', '739123', 'Afkar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `dimention` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `stock`, `price`, `dimention`, `description`, `image`, `is_deleted`) VALUES
(1, 'Productku', 10, 120000, 100, 'Deskripsi Produk1', 'man-13.png', 0),
(3, 'Product Satunya', 2, 20000, 12, 'Deskripsi Produk', 'product-91.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`, `is_deleted`) VALUES
(1, 'Customer', 0),
(2, 'CEO', 0),
(3, 'Specialized HRD Staff', 0),
(4, 'Specialized Finance Staff', 0),
(5, 'Production Manager', 0),
(6, 'Marketing and sales staff', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`detail_order_id`);

--
-- Indexes for table `global_variable`
--
ALTER TABLE `global_variable`
  ADD PRIMARY KEY (`global_variable_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `detail_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `global_variable`
--
ALTER TABLE `global_variable`
  MODIFY `global_variable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
