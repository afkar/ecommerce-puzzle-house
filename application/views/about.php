<section class="contact-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="contact-title">
                    <h4>Contacts Us</h4>
                </div>
                <div class="contact-widget">
                    <div class="cw-item">
                        <div class="ci-icon">
                            <i class="ti-location-pin"></i>
                        </div>
                        <div class="ci-text">
                            <span>Address:</span>
                            <p>Jl. Mayang, Desa Tegalluar, Kec. Bojongsoang,</p>
                        </div>
                    </div>
                    <div class="cw-item">
                        <div class="ci-icon">
                            <i class="ti-user"></i>
                        </div>
                        <div class="ci-text">
                            <span>Owner :</span>
                            <p>Naufal Supriyanto Tarigan</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 offset-lg-1">
                <div class="contact-form">
                    <div class="leave-comment">
                        <img src="<?=base_url('asset/img/aboutus.png')?>" style="width: 800px;" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>