<section class="hero-section">
	<div class="hero-items owl-carousel">
		<?php foreach ($dataProduct as $product): ?>
		<div class="single-hero-items set-bg" data-setbg="<?=base_url('asset/gambar_product/'.$product->image )?>" style="background-position: right center; background-size: cover;">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<h1><?= $product->product_name ?></h1>
						<?php if($product->product_id == "1"){
							echo "<p>Doll House Corp merupakan produsen mainan edukatif yang memproduksi Puzzle House. Puzzle House merupakan sebuah mainan edukatif untuk anak berusia 4-7 tahun yang berbentuk layaknya seperti rumah dan perabotannya umumnya.</p>";
						}else{
							echo "<p>Puzzle House memiliki panjang 45cm, lebar 25 cm, dan tinggi 35cm. Puzzle akan dijual dimulai dengan harga Rp 250.000 - Rp 325.000.</p>";
						}?>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach ?>
	</div>
</section>