<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $judul ?></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <?=$this->session->flashdata('pesan');?>
    <center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Id</th>
            <th>Fullname</th>
            <th>Adress</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Username</th>
            <th>Password</th>
            <th>Role</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($dataAccount as $account): ?>
            <tr>
                <td><?=$account->account_id?></td>
                <td><?=$account->full_name?></td>
                <td><?=$account->address?></td>
                <td><?=$account->phone?></td>
                <td><?=$account->email?></td>
                <td><?=$account->username?></td>
                <td><?=$account->password?></td>
                <td><?=$account->role_name?></td>
                <td align="center">
                    <a href="#edit" onclick="edit(<?=$account->account_id?>)" data-toggle="modal" class="btn btn-success" style="width: 90px; margin: 10px 0px">Ubah</a> 
                    <a href="<?=base_url('index.php/master_account/hapus/'.$account->account_id)?>" onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-danger" style="width: 90px;">Hapus</a>
                </td>
            </tr>
		    <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit account</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/master_account/tambah')?>" method="post" enctype="multipart/form-data">
          <table>
            <tr>
              <td>Fullname</td><td><input required type="text" name="full_name" class="form-control"></td>
            </tr>
            <tr>
              <td>Address</td><td><input type="text" required name="address" class="form-control"></td>
            </tr>
            <tr>
              <td>Phone</td><td><input type="number" required name="phone" class="form-control"></td>
            </tr>
            <tr>
              <td>Email</td><td><input type="email" required name="email" class="form-control"></td>
            </tr>
            <tr>
              <td>Username</td><td><input type="text" required name="username" class="form-control"></td>
            </tr>
            <tr>
            <tr>
              <td>Password</td><td><input type="password" required name="password" class="form-control"></td>
            </tr>
            <tr>
            <tr>
              <td>Role</td><td><select name="role" class="form-control">
      				<option></option>
      				<?php foreach ($role as $r): ?>
      					<option value="<?=$r->role_id?>">
      						<?=$r->role_name ?></option>
      				<?php endforeach ?>
      			</select></td>
            </tr>
            <tr>
            <tr>
              <td>is_deleted</td>
              <td>
                  <select name="is_deleted" class="form-control">
                      <option value="0">False</option>
                      <option value="1">True</option>
                  </select>
            </td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Account</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/master_account/account_update')?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="account_id" id="account_id">
          <table>
            <tr>
              <td>Fullname</td><td><input id="full_name" required type="text" name="full_name" class="form-control"></td>
            </tr>
            <tr>
              <td>Address</td><td><input type="text" id="address" required name="address" class="form-control"></td>
            </tr>
            <tr>
              <td>Phone</td><td><input type="number" id="phone" required name="phone" class="form-control"></td>
            </tr>
            <tr>
              <td>Email</td><td><input type="email" id="email" required name="email" class="form-control"></td>
            </tr>
            <tr>
              <td>Username</td><td><input type="text" id="username" required name="username" class="form-control"></td>
            </tr>
            <tr>
            <tr>
              <td>Password</td><td><input type="password" id="password" required name="password" class="form-control"></td>
            </tr>
            <tr>
            <tr>
              <td>Role</td><td><select name="role" required id="role" class="form-control">
      				<option></option>
      				<?php foreach ($role as $r): ?>
      					<option value="<?=$r->role_id?>">
      						<?=$r->role_name ?></option>
      				<?php endforeach ?>
      			</select></td>
            </tr>
            <tr>
            <tr>
              <td>is_deleted</td>
              <td>
                  <select name="is_deleted" required id="is_deleted" class="form-control">
                      <option value="0">False</option>
                      <option value="1">True</option>
                  </select>
            </td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="edit" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/master_account/edit_account/"+a, 
       dataType:"json",
       success:function(data){
        $("#account_id").val(data.account_id);
        $("#full_name").val(data.full_name);
        $("#address").val(data.address);
        $("#phone").val(data.phone);
        $("#email").val(data.email);
        $("#username").val(data.username);
        $("#password").val(data.password);
        $("#role").val(data.role_id);
        $("#is_deleted").val(data.is_deleted);
      }
      });
    }
</script>