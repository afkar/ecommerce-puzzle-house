<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $judul ?></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <?=$this->session->flashdata('pesan');?>
    <center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Id Product</th>
            <th>Name</th>
            <th>Stock</th>
            <th>Price</th>
            <th>Dimention</th>
            <th>Description</th>
            <th>Image</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($dataProduct as $product): ?>
            <tr>
                <td><?=$product->product_id?></td>
                <td><?=$product->product_name?></td>
                <td><?=$product->stock?></td>
                <td><?= number_format($product->price) ?></td>
                <td><?=$product->dimention?></td>
                <td><?=$product->description?></td>
                <td><img src="<?=base_url('asset/gambar_product/'.$product->image )?>" style="width:40px"></td>
                <td align="center">
                    <a href="#edit" onclick="edit(<?=$product->product_id?>)" data-toggle="modal" class="btn btn-success" style="width: 90px; margin: 10px 0px">Ubah</a> 
                    <a href="<?=base_url('index.php/pm/hapus/'.$product->product_id)?>" onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-danger" style="width: 90px;">Hapus</a>
                </td>
            </tr>
		    <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Product</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/master_account/tambah')?>" method="post" enctype="multipart/form-data">
          <table>
            <tr>
              <td>Product Name</td><td><input required type="text" name="product_name" class="form-control"></td>
            </tr>
            <tr>
              <td>Price</td><td><input type="number" required name="price" class="form-control"></td>
            </tr>
            <tr>
              <td>Stock</td><td><input type="number" required name="stock" class="form-control"></td>
            </tr>
            <tr>
              <td>Dimention</td><td><input name="dimention" required class="form-control" type="text"></td>
            </tr>
            <tr>
              <td>Deskripsi</td><td><textarea name="deskripsi" required class="form-control"></textarea></td>
            </tr>
            <input type="hidden" name="is_deleted" value="0">
            <tr>
              <td>Gambar</td><td><input type="file" name="gambar" class="form-control"></td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Product</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/pm/product_update')?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="product_id" id="product_id">
          <table>
            <tr>
              <td>Product Name</td><td><input id="product_name" required type="text" name="product_name" class="form-control"></td>
            </tr>
            <tr>
              <td>Price</td><td><input type="number" id="price" required name="price" class="form-control"></td>
            </tr>
            <tr>
              <td>Stock</td><td><input type="number" id="stock" required name="stock" class="form-control"></td>
            </tr>
            <tr>
              <td>Dimention</td><td><input id="dimention" name="dimention" required class="form-control" type="text"></td>
            </tr>
            <tr>
              <td>Deskripsi</td><td><textarea id="deskripsi" name="deskripsi" required class="form-control"></textarea></td>
            </tr>
            <tr>
              <td>is_deleted</td>
              <td>
                  <select name="is_deleted" required id="is_deleted" class="form-control">
                      <option value="0">False</option>
                      <option value="1">True</option>
                  </select>
              </td>
            </tr>
            <tr>
              <td>Gambar</td><td><input type="file" name="gambar" id="gambar" class="form-control"></td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/pm/edit_product/"+a, 
       dataType:"json",
       success:function(data){
        $("#product_id").val(data.product_id);
        $("#product_name").val(data.product_name);
        $("#stock").val(data.stock);
        $("#price").val(data.price);
        $("#dimention").val(data.dimention);
        $("#deskripsi").val(data.description);
        $("#is_deleted").val(data.is_deleted);
      }
      });
    }
</script>