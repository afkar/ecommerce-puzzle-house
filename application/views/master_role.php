<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800"><?= $judul ?></h1>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <?=$this->session->flashdata('pesan');?>
    <center><a href="#tambah" data-toggle="modal" class="btn btn-warning">Tambah</a></center>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-broleed" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Id</th>
            <th>Role Name</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($dataRole as $role): ?>
            <tr>
                <td><?=$role->role_id?></td>
                <td><?=$role->role_name?></td>
                <td>
                    <a href="#edit" onclick="edit(<?=$role->role_id?>)" data-toggle="modal" class="btn btn-success" style="width: 90px; margin: 10px 0px">Ubah</a> 
                    <a href="<?=base_url('index.php/master_role/hapus/'.$role->role_id)?>" onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-danger" style="width: 90px;">Hapus</a>
                </td>
            </tr>
		    <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->

<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Role</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/master_role/tambah')?>" method="post" enctype="multipart/form-data">
          <table>
            <tr>
                <td>Role Name</td>
                <td><input required type="text" name="role_name" class="form-control"></td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="simpan" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit role</h4>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/master_role/role_update')?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="role_id" id="role_id">
          <table>
            <tr>
              <td>Role Name</td><td><input required type="text" name="role_name" id="role_name" class="form-control"></td>
            </tr>
          </table>
          <br>
          <div>
            <input type="submit" name="edit" value="Simpan" class="btn btn-success">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/master_role/edit_role/"+a, 
       dataType:"json",
       success:function(data){
        $("#role_id").val(data.role_id);
        $("#role_name").val(data.role_name);
      }
      });
    }
</script>