<!-- Breadcrumb Section Begin -->
<div class="breacrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <a href="<?=base_url('index.php/dashboard')?>"><i class="fa fa-home"></i> Home</a>
                        <span>Shop</span>
                        <?php 
                            if($this->session->flashdata('pesan')!=null){
                                echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

    <!-- Product Shop Section Begin -->
    <section class="product-shop spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-1 order-lg-2">
                    <div class="product-list">
                        <div class="row">
                            <?php foreach($dataProduct as $product): ?>
                            <div class="col-lg-4 col-sm-6">
                                <div class="product-item">
                                    <div class="pi-pic">
                                        <img src="<?=base_url('asset/gambar_product/'.$product->image )?>" alt="" style="width: 100px; height: 300px">
                                        <!-- <div class="sale pp-sale">Sale</div> -->
                                        <div class="icon">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul>
                                            <li class="quick-view"><a href="<?=base_url('index.php/product/detailProduct/'.$product->product_id)?>">+ Quick View</a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name">Towel</div>
                                        <a href="<?=base_url('index.php/product/detailProduct/'.$product->product_id)?>">
                                            <h5><?= $product->product_name ?></h5>
                                        </a>
                                        <div class="product-price">
                                            <?= number_format($product->price) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Shop Section End -->