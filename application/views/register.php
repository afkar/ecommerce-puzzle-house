<div class="register-login-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="register-form">
                <?php 
                if($this->session->flashdata('pesan')!=null){
                echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";}?>
                    <h2>Register</h2>
                    <form action="<?=base_url('index.php/dashboard/simpan')?>" method="post">
                        <div class="group-input">
                            <label for="full_name">Fullname *</label>
                            <input type="text" name="full_name">
                        </div>
                        <div class="group-input">
                            <label for="email">Email Address *</label>
                            <input type="email" name="email">
                        </div>
                        <div class="group-input">
                            <label for="username">Username *</label>
                            <input type="text" name="username">
                        </div>
                        <div class="group-input">
                            <label for="pass">Password *</label>
                            <input type="password" name="password">
                        </div>
                        <div class="group-input">
                            <label for="con-pass">Address</label>
                            <input type="text" name=address>
                        </div>
                        <div class="group-input">
                            <label for="con-pass">Phone</label>
                            <input type="number" name="phone">
                        </div>
                        <input type="submit" class="site-btn register-btn" value="REGISTER" name="submit">
                    </form>
                    <!-- <div class="switch-login">
                        <a href="./login.html" class="or-login">Or Login</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>