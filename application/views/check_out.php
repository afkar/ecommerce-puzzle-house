<section class="checkout-section spad">
    <div class="container">
        <form action="<?=base_url('index.php/checkout/confirmBuy')?>" class="checkout-form" method="post">
            <div class="row">
                <div class="col-lg-6">
                    <h4>Biiling Details</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="fir">Full Name<span>*</span></label>
                            <input type="text" disabled value="<?= $this->session->userdata('full_name') ?>">
                        </div>
                        <div class="col-lg-12">
                            <label for="street">Street Address<span>*</span></label>
                            <input type="text" class="street-first" disabled value="<?= $this->session->userdata('address') ?>">
                        </div>
                        <div class="col-lg-6">
                            <label for="email">Email Address<span>*</span></label>
                            <input type="text" disabled value="<?= $this->session->userdata('email') ?>">
                        </div>
                        <div class="col-lg-6">
                            <label for="phone">Phone<span>*</span></label>
                            <input type="text" disabled value="<?= $this->session->userdata('phone') ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="place-order">
                        <h4>Your Order</h4>
                        <div class="order-total">
                            <ul class="order-table">
                                <li>Product <span>Total</span></li>
                                <li class="fw-normal" style="padding: 10px;"><img src="<?=base_url('asset/gambar_product/'.$this->session->userdata('image') )?>" alt="" style="width: 70px; height: 100px">  <?= $this->session->userdata('product_name') ?> x <?= $this->input->post('qty') ?> <span style="margin-top: 37px;"><?= number_format($this->session->userdata('ammount')) ?>/product</span></li>
                                <li class="total-price" style="padding: 10px;">Total <span><?= number_format($dataAmount) ?></span></li>
                                <div class="product-show-option">
                                    <div class="row">
                                        <div class="col-lg-7 col-md-7">
                                            <li style="padding: 10px;">Payment Method: 
                                                <div class="select-option" style="margin: 10px 0px;">
                                                    <select class="sorting" name="payment">
                                                        <?php foreach($dataPayment as $p): ?>
                                                            <option value="<?= $p->payment_id ?>" style="padding: 0px;"><?= $p->payment_name ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                            <div class="order-btn">
                                <input type="submit" class="site-btn place-btn" value="Place Order">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>