<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Order extends CI_Model {

    public function get_order()
    {
        return $this->db
            ->join('account','account.account_id=order.account_id')
            ->join('product','product.product_id=order.product_id')
            ->join('payment','payment.payment_id=order.payment_id')
            ->join('global_variable','global_variable.global_variable_id=order.status_id')
            ->get('order')
            ->result();
    }

	public function get_status()
	{
		return $this->db
					->get('global_variable')->result();
	}

	public function detail_order($id)
	{
		return $this->db
					->where('order_id',$id)
					->get('order')
					->row();
	}

	public function update_order()
	{
		$object=array(
		'status_id'=>$this->input->post('status'),
		);
		return $this->db
					->where('order_id',$this->input->post('order_id'))
					->update('order', $object);
	}

	public function hapus_order($id)
	{
		return $this->db->where('order_id',$id)->delete('order');
	}

}

/* End of file M_Order.php */

?>