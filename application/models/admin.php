<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Model
{
    //fungsi cek session logged in
    function is_logged_in()
    {
        return $this->session->userdata('user_id');
    }   
    function check_account($table,$email,$password,$error,$dateNow){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($email);
        $this->db->where($password);
        $this->db->where($dateNow);
        $this->db->limit(1);
        $query = $this->db->get();     
           
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();          
        }
    }

    // ACCOUNT

    /**
     * Function for Register
    */
    public function register($data) {
        $this->db->insert('account', $data);
        return $this->db->insert_id();
    }

    /**
     * Function for Get Account by Username and Email
    */
    public function getAccount($username,$email)
    {   
        $sql = $this->db->query("SELECT * FROM account WHERE (username = '$username' OR email = '$email') AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get Account by Account Id
    */
    public function getAccountById($account_id)
    {   
        $sql = $this->db->query("SELECT * FROM account WHERE account_id = $account_id AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get List Account
    */
    public function getListAccount()
    {   
        $sql = $this->db->query("SELECT * FROM account WHERE is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Update Account
    */
    public function updateAccount($account_id, $full_name, $address, $phone, $email, $username, $password, $role_id)
    {   
        $sql = $this->db->query("UPDATE account 
                                    SET full_name = '$full_name',
                                        address = '$address', 
                                        phone = '$phone',
                                        email = '$email',
                                        username = '$username',
                                        password = '$password',
                                        role_id = $role_id
                                WHERE account_id = $account_id AND is_deleted = 0");    
        return $sql;
    }

    /**
     * Function for Reset Password
    */
    public function resetPassword($account_id, $newPassword)
    {   
        $sql = $this->db->query("UPDATE account 
                                    SET password = '$newPassword'                                        
                                WHERE account_id = $account_id AND is_deleted = 0");
        return $sql;  
    }

    /**
     * Function for Delete Account
    */
    public function deleteAccount($account_id)
    {   
        $sql = $this->db->query("UPDATE account SET is_deleted = 1 WHERE account_id = $account_id");
        return $sql;
    }
    


    // ROLE

    /**
     * Function for Add Role
    */
    public function addRole($data) {
        $this->db->insert('role', $data);
        return $this->db->insert_id();
    }

    /**
     * Function for Get Role By Id
    */
    public function getRoleById($role_id)
    {   
        $sql = $this->db->query("SELECT * FROM role WHERE role_id = $role_id AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get List Role
    */
    public function getListRole()
    {   
        $sql = $this->db->query("SELECT * FROM role WHERE is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Update Role
    */
    public function updateRole($role_id, $role_name)
    {   
        $sql = $this->db->query("UPDATE role SET role_name = '$role_name' WHERE role_id = $role_id AND is_deleted = 0");
        return $sql;
    }

    /**
     * Function for Delete Role
    */
    public function deleteRole($role_id)
    {   
        $sql = $this->db->query("UPDATE role SET is_deleted = 1 WHERE role_id = $role_id");
        return $sql;
    }



    // GLOBAL Variabel
    
    /**
     * Function for Add Global Variable
    */
    public function addGlobalVariable($data) {
        $this->db->insert('global_variable', $data);
        return $this->db->insert_id();
    }

    /**
     * Function for Get Global Variable By Type
    */
    public function getGlobalVariableByType($type)
    {   
        $sql = $this->db->query("SELECT * FROM global_variable WHERE type = $type AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get Global Variable Status Order
    */
    public function getStausOrder($value)
    {   
        $sql = $this->db->query("SELECT * FROM global_variable WHERE type = 'Status' AND value = $value AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get Global Variable By Id/Value
    */
    public function getGlobalVariableById($value)
    {   
        $sql = $this->db->query("SELECT * FROM global_variable WHERE value = $value AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get List Global Variable 
    */
    public function getListGlobalVariable()
    {   
        $sql = $this->db->query("SELECT * FROM global_variable WHERE is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Update Global Variable
    */
    public function updateGlobalVariable($global_variable_id, $type ,$value,$description)
    {   
        $sql = $this->db->query("UPDATE global_variable 
                                    SET type = '$type',
                                        value = $value,
                                        description = '$description'
                                WHERE global_variable_id = $global_variable_id AND is_deleted = 0");
        return $sql;
    }

    /**
     * Function for Delete Global Variable
    */
    public function deleteGlobalVariable($global_variable_id)
    {   
        $sql = $this->db->query("UPDATE global_variable SET is_deleted = 1 WHERE global_variable_id = $global_variable_id");
        return $sql;
    }


    // PAYMENT
    
    /**
     * Function for Add Payment
    */
    public function addPayment($data) {
        $this->db->insert('payment', $data);
        return $this->db->insert_id();
    }

    /**
     * Function for Get Payment By Id
    */
    public function getPaymentById($payment_id)
    {   
        $sql = $this->db->query("SELECT * FROM payment WHERE payment_id = $payment_id AND is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get List Payment 
    */
    public function getListPayment()
    {   
        $sql = $this->db->query("SELECT * FROM payment WHERE is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Update Payment
    */
    public function updatePayment($payment_id, $payment_name ,$member_number, $member_name)
    {   
        $sql = $this->db->query("UPDATE payment 
                                    SET payment_name = '$payment_name',
                                        member_number = '$member_number',
                                        member_name = '$member_name'
                                WHERE payment_id = $payment_id AND is_deleted = 0");
        return $sql;
    }

    /**
     * Function for Delete Payment
    */
    public function deletePayment($payment_id)
    {   
        $sql = $this->db->query("UPDATE payment SET is_deleted = 1 WHERE payment_id = $payment_id");
        return $sql;
    }
    // ORDER
    
    /**
     * Function for Add Order
    */
    public function addOrder($data) {           
        $dataOrder = array(
                "order_id" => '',
                "account_id" => $data['account_id'],
                "product_id" => $data['product_id'],
                "payment_id" => $data['payment_id'],
                "amount" => $data['amount'],
                "output" => $data['output'],
                "order_date" => $data['order_date'],
                "status_id" => $data['status_id'],
                "is_deleted" => 0   
        );
        $this->db->insert('order', $dataOrder);
        $order_id = $this->db->insert_id();        
        $dataOrderDetail = array(
                "detail_order_id" => '',
                "order_id" => $order_id,
                "username" => $data['username'],
                "product_name" => $data['product_name'],
                "price" => $data['price'],
                "payment_name" => $data['payment_name'],
                "output" => $data['output'],
                "address" => $data['address'],
                "is_deleted" => 0   
        );        
        $this->db->insert('detail_order', $dataOrderDetail);
        return $this->db->insert_id();        
    }

    /**
     * Function for Get Detail Order By Account Id
    */
    public function getDetailOrderByAccountId($account_id)
    {   
        $sql = $this->db->query("SELECT 
                                    a.order_id AS order_id,
                                    b.username AS username,
                                    b.product_name AS product_name,
                                    b.price AS price,
                                    b.payment_name AS payment_name,
                                    b.output AS output,
                                    a.amount AS amount,
                                    a.order_date AS order_date,
                                    b.address AS address
                                FROM `order` a 
                                JOIN detail_order b ON a.order_id = b.order_id
                                WHERE a.account_id = $account_id AND a.is_deleted = 0 AND b.is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Get List Detail Order 
    */
    public function getListDetailOrder()
    {   
        $sql = $this->db->query("SELECT 
                                    a.order_id AS order_id,
                                    b.username AS username,
                                    b.product_name AS product_name,
                                    b.price AS price,
                                    b.payment_name AS payment_name,
                                    b.output AS output,
                                    a.amount AS amount,
                                    a.order_date AS order_date,
                                    b.address AS address
                                FROM `order` a 
                                INNER JOIN detail_order b ON a.order_id = b.order_id
                                WHERE a.is_deleted = 0 AND b.is_deleted = 0");
        return $sql->result();
    }

    /**
     * Function for Update Status Order
    */
    public function updateStatusOrder($order_id,$status_id)
    {   
        $sql = $this->db->query("UPDATE `order` SET status_id = $status_id WHERE order_id = $order_id AND is_deleted = 0");
        return $sql;
    }

    /**
     * Function for Delete Order
    */
    public function deleteOrder($order_id)
    {   
        $sql = $this->db->query("UPDATE `order` SET is_deleted = 1 WHERE order_id = $order_id");
        $sql = $this->db->query("UPDATE detail_order SET is_deleted = 1 WHERE order_id = $order_id");
        return $sql;
    }





}
?>