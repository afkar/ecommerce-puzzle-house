<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Payment extends CI_Model {

    public function get_payment()
    {
        return $this->db
            ->get('payment')
            ->result();
    }

	public function simpan_payment()
	{
		$payment_name=$this->input->post('payment_name');
		$member_number=$this->input->post('member_number');
		$member_name=$this->input->post('member_name');
		$data_simpan=array(
			'payment_name'=>$payment_name,
			'member_number'=>$member_number,
			'member_name'=>$member_name,
			'is_deleted'=>0
			);
		$this->db->insert('payment',$data_simpan);
	}

	public function detail_payment($id)
	{
		return $this->db
					->where('payment_id',$id)
					->get('payment')
					->row();
	}

	public function update_payment()
	{
		$object=array(
		'payment_name'=>$this->input->post('payment_name'),
        'member_number'=>$this->input->post('member_number'),
        'member_name'=>$this->input->post('member_name')
		);
		return $this->db
					->where('payment_id',$this->input->post('payment_id'))
					->update('payment', $object);
	}

	public function hapus_payment($id)
	{
		return $this->db->where('payment_id',$id)->delete('payment');
	}

}

/* End of file M_Payment.php */

?>