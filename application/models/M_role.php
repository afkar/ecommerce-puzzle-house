<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Role extends CI_Model {

    public function get_role()
    {
        return $this->db
            ->get('role')
            ->result();
    }

	public function simpan_role()
	{
		$role_name=$this->input->post('role_name');
		$data_simpan=array(
			'role_name'=>$role_name,
			'is_deleted'=>0
			);
		$this->db->insert('role',$data_simpan);
	}

	public function detail_role($id)
	{
		return $this->db
					->where('role_id',$id)
					->get('role')
					->row();
	}

	public function update_role()
	{
		$object=array(
		'role_name'=>$this->input->post('role_name'),
		);
		return $this->db
					->where('role_id',$this->input->post('role_id'))
					->update('role', $object);
	}

	public function hapus_role($id)
	{
		return $this->db->where('role_id',$id)->delete('role');
	}

}

/* End of file M_Role.php */

?>