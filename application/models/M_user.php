<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

    /**
     * Function for Register
    */
    public function register($data) {
        $this->db->insert('account', $data);
        return $this->db->insert_id();
    }

	public function masuk()
    {
		$email=$this->input->post('email');
		$full_name=$this->input->post('full_name');
		$address=$this->input->post('address');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$phone=$this->input->post('phone');
		$data_simpan=array(
			'email'=>$email,
			'full_name'=>$full_name,
			'address'=>$address,
			'username'=>$username,
			'password'=>$password,
			'phone'=>$phone,
			'role_id'=>1
			);
		$this->db->insert('account',$data_simpan);
		if($this->db->affected_rows()>0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function resetPW()
	{
		$object=array(
		'password'=>$this->input->post('resetPassword')
		);
		return $this->db
			->where('username',$this->input->post('resetUsername'))
			->update('account', $object);
	}

	public function get_login()
	{
		$password = $this->input->post('password');
		return $this->db
				->where('username',$this->input->post('username'))
				->where('password',$this->input->post('password'))
				->get('account');
				// ->where('password',md5($password))
	}

	public function get_loginAdmin()
	{
		$password = $this->input->post('password');
		return $this->db
				->where('username',$this->input->post('usernameAdmin'))
				->where('password',$this->input->post('passwordAdmin'))
				->get('account');
				// ->where('password',md5($password))
	}

	public function get_account()
	{
		return $this->db
					->join('role','role.role_id=account.role_id')
					->get('account')->result();
	}

	public function get_roles()
	{
		return $this->db->get('role')->result();
	}

	public function get_role()
	{
		return $this->db
				->where('role_id',$this->session->userdata('role_idAdmin'))
				->get('role');
	}

	public function detail_profil($account_id)
	{
		return $this->db
					->where('account_id',$account_id)
					->get('account')
					->row();
	}

	public function update_account()
	{
		$object=array(
		'full_name'=>$this->input->post('full_name'),
		'address'=>$this->input->post('address'),
		'phone'=>$this->input->post('phone'),
		'email'=>$this->input->post('email'),
		'username'=>$this->input->post('username'),
		'password'=>$this->input->post('password'),
		'role_id'=>$this->input->post('role'),
		'is_deleted'=>$this->input->post('is_deleted')
		);
		return $this->db
					->where('account_id',$this->input->post('account_id'))
					->update('account', $object);
	}

	public function simpan_user()
	{
		$email=$this->input->post('email');
		$full_name=$this->input->post('full_name');
		$address=$this->input->post('address');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$phone=$this->input->post('phone');
		$role=$this->input->post('role');
		$data_simpan=array(
			'email'=>$email,
			'full_name'=>$full_name,
			'address'=>$address,
			'username'=>$username,
			'password'=>$password,
			'phone'=>$phone,
			'role_id'=>$role,
			'is_deleted'=>0
			);
		$this->db->insert('account',$data_simpan);
	}

	public function hapus_user($id_p='')
	{
		return $this->db->where('account_id',$id_p)->delete('account');
	}
}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */