<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_product extends CI_Model {

	public function getListProduct()
	{
		$result=$this->db
					 ->where('is_deleted',0)
					 ->get('product')->result();
		return $result;
	}

	public function getProduct2()
	{
		$result=$this->db
					 ->where('is_deleted',0)
					 ->limit(2)
					 ->get('product')->result();
		return $result;
	}

    /**
     * Function for Get Product By Id
    */
    public function getProductById($product_id)
    {   
		$result=$this->db
					 ->where('product_id',$product_id)
					 ->get('product')
					 ->row();
		return $result;
	}
	
	public function simpan_product($nama_file)
	{
		if($nama_file==""){
			$object=array(
                'product_name'=>$this->input->post('product_name'),
                'stock'=>$this->input->post('stock'),
                'price'=>$this->input->post('price'),
                'dimention'=>$this->input->post('dimention'),
                'description'=>$this->input->post('deskripsi'),
                'is_deleted'=>$this->input->post('is_deleted')
			);	
		} else{
			$object=array(
                'product_name'=>$this->input->post('product_name'),
                'stock'=>$this->input->post('stock'),
                'price'=>$this->input->post('price'),
                'dimention'=>$this->input->post('dimention'),
                'description'=>$this->input->post('deskripsi'),
                'is_deleted'=>$this->input->post('is_deleted'),
                'image'=>$nama_file
			);
		}
		return $this->db->insert('product', $object);
	}

	public function product_update_dengan_foto($nama_foto='')
	{
		$object=array(
			'product_name'=>$this->input->post('product_name'),
			'stock'=>$this->input->post('stock'),
			'price'=>$this->input->post('price'),
			'dimention'=>$this->input->post('dimention'),
			'description'=>$this->input->post('deskripsi'),
			'is_deleted'=>$this->input->post('is_deleted'),
			'image'=>$nama_foto
		);
		return $this->db->where('product_id',$this->input->post('product_id'))
					->update('product', $object);	
	}
	public function product_update_no_foto()
	{
		$object=array(
			'product_name'=>$this->input->post('product_name'),
			'stock'=>$this->input->post('stock'),
			'price'=>$this->input->post('price'),
			'dimention'=>$this->input->post('dimention'),
			'description'=>$this->input->post('deskripsi'),
			'is_deleted'=>$this->input->post('is_deleted')
		);
		return $this->db->where('product_id',$this->input->post('product_id'))
					->update('product', $object);
	}

	public function hapus_product($id_p='')
	{
		return $this->db->where('product_id',$id_p)->delete('product');
	}

}

/* End of file ModelName.php */

?>