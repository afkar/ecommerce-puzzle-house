<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_ConfirmBuy extends CI_Model {
    
        public function prosesOrder($a)
        {
            date_default_timezone_set('Asia/Jakarta');
            $payment_id = $this->input->post('payment');
            
            $data = array(
                'account_id' => $this->session->userdata('account_id'),
                'product_id' => $this->session->userdata('product_id'),
                'payment_id' => $a,
                'amount' => $this->session->userdata('ammount') * $this->session->userdata('qty'),
                'output' => $this->session->userdata('qty'),
                'order_date' => date('Y-m-d H:i:s'),
                'status_id' => 1,
                'is_deleted' => 0
            );
            $this->session->set_userdata($data);
            
            $this->db->insert('order', $data);
            
            if($this->db->affected_rows()>0){
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function get_order()
        {
            return $this->db
                        ->join('account','account.account_id=order.account_id')
                        ->join('product','product.product_id=order.product_id')
                        ->join('payment','payment.payment_id=order.payment_id')
                        ->join('global_variable','global_variable.global_variable_id=order.status_id')
                        ->where('order_date', $this->session->userdata('order_date'))
                        ->get('order')
                        ->row();
            
        }
    
    }
    
    /* End of file ConfirmBuy.php */
    
?>