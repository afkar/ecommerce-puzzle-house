<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Role extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_role','role');
    }
    

    public function index()
    {
        $data['dataRole']=$this->role->get_role();
        $data['konten']='master_role';
        $data['judul']='Data Role';
        $this->load->view('admin_dashboard', $data);
    }

	public function tambah()
	{
		if($this->input->post('simpan')){
			if($this->role->simpan_role()){
				$this->session->set_flashdata('pesan', 'Sukses menambahkan');
				redirect('master_role','refresh');
			} else {
				$this->session->set_flashdata('pesan', 'Gagal menambahkan');
				redirect('master_role','refresh');
			}
		}
	}

    public function edit_role($id)
    {
        $data=$this->role->detail_role($id);
        echo json_encode($data);
    }

	public function role_update()
	{
		if($this->input->post('edit')){
			if($this->role->update_role()){
			$this->session->set_flashdata('pesan', 'Sukses Update');
			redirect('master_role','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Hapus');
			redirect('master_role','refresh');	
		}
		}
	}

	public function hapus($id)
	{
		if($this->role->hapus_role($id)){
            $this->session->set_flashdata('pesan', 'Sukses menghapus');
            redirect('master_role','refresh');
        } else {
            $this->session->set_flashdata('pesan', 'Gagal menghapus');
            redirect('master_role','refresh');
        }
	}

}

/* End of file Master_Role.php */

?>