<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Pm extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_product','product');
            
        }
        
    
        public function index()
        {
            $data['dataProduct']=$this->product->getListProduct();
            $data['konten']='data_product';
            $data['judul']='Data Product';
            $this->load->view('admin_dashboard', $data);
        }

        public function edit_product($id)
        {
            $data=$this->product->getProductById($id);
            echo json_encode($data);
        }
        
	public function product_update()
	{
		if($this->input->post('simpan')){
			if($_FILES['gambar']['name']==""){
				if($this->product->product_update_no_foto()){
					$this->session->set_flashdata('pesan', 'Sukses update');
					redirect('pm');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal update');
					redirect('pm');
				}
			} else {
				$config['upload_path'] = './asset/gambar_product/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']  = '20000';
				$config['max_width']  = '5024';
				$config['max_height']  = '5768';
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', 'Gagal Upload');
					redirect('pm');
				}
				else{
					if($this->product->product_update_dengan_foto($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'Sukses update');
						redirect('pm');
					} else {
						$this->session->set_flashdata('pesan', 'Gagal update');
						redirect('pm');
					}
				}
			}
			
		}

    }
    
	public function tambah()
	{
		$this->form_validation->set_rules('product_name', 'nama product', 'trim|required');
		$this->form_validation->set_rules('price', 'harga', 'trim|required');
		$this->form_validation->set_rules('stock', 'deskripsi', 'trim|required');
		$this->form_validation->set_rules('dimention', 'jenis', 'trim|required');
		$this->form_validation->set_rules('is_deleted', 'jenis', 'trim|required');
		if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './asset/gambar_product/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '100000';
			$config['max_width']  = '5024';
			$config['max_height']  = '4768';
			if($_FILES['gambar']['name']!=""){
				$this->load->library('upload', $config);
			
				if ( ! $this->upload->do_upload('gambar')){
					$this->session->set_flashdata('pesan', $this->upload->display_errors());
				}
				else{
					if($this->product->simpan_product($this->upload->data('file_name'))){
						$this->session->set_flashdata('pesan', 'sukses menambah');	
					} else {
						$this->session->set_flashdata('pesan', 'gagal menambah');	
					}
					redirect('pm','refresh');		
				}
			} else {
				if($this->product->simpan_product('')){
					$this->session->set_flashdata('pesan', 'sukses menambah');	
				} else {
					$this->session->set_flashdata('pesan', 'gagal menambah');	
				}
				redirect('pm','refresh');	
			}
			
		} else {
			$this->session->set_flashdata('pesan', validation_errors());
			redirect('pm','refresh');
		}

	}
    
	public function hapus($id_product='')
	{
		if($this->product->hapus_product($id_product)){
			$this->session->set_flashdata('pesan', 'Sukses Hapus product');
			redirect('pm','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Hapus product');
			redirect('pm','refresh');	
		}
	}
    
    }
    
    /* End of file Controllername.php */
    
?>