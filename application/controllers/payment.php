<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('admin');
    }
    
    public function index(){
        // $this->deleteAccount();
        // $result = $this->admin->getListAccount();
        // print_r($result);
    }

    /**
     * Function for Add Payment
    */
    public function addPayment()
    {
        $payment_name = "Ovo";
        $member_number = 876555117;
        $member_name = "Muhammad Razak";        
        
        $data = array (
                "payment_id" => '',
                "payment_name" => $payment_name,
                "member_number" => $member_number,
                "member_name" => $member_name,                
                "is_deleted" => 0   
            );
        $result =  $this->admin->addPayment($data);
        return $result;        
    }

    /**
     * Function for Update Payment
    */
    public function updatePayment()
    {
        $payment_id = 1;
        $payment_name = "Ovo";
        $member_number = 876555117;
        $member_name = "Muhammad Razak";       
        $this->admin->updatePayment($payment_id,$payment_name,$member_number,$member_name);                                    

    }
    
    /**
     * Function for Delete Payment
    */
    public function deletePayment()
    {                        
        $payment_id = 1;         
        $this->admin->deletePayment($payment_id);                                            
    }

    
}
?>