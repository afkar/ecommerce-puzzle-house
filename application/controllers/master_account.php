<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Account extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_user','user');        
    }
    

    public function index()
    {
        $data['dataAccount']=$this->user->get_account();
        $data['role']=$this->user->get_roles();
        $data['konten']='master_account';
        $data['judul']='Data Account';
        $this->load->view('admin_dashboard', $data);
    }

    public function edit_account($id)
    {
        $data=$this->user->detail_profil($id);
        echo json_encode($data);
    }

	public function tambah()
	{
		if($this->input->post('simpan')){
			if($this->user->simpan_user()){
				$this->session->set_flashdata('pesan', 'Sukses menambahkan');
				redirect('master_account','refresh');
			} else {
				$this->session->set_flashdata('pesan', 'Gagal menambahkan');
				redirect('master_account','refresh');
			}
		}
	}

	public function hapus($id='')
	{
		if($this->user->hapus_user($id)){
            $this->session->set_flashdata('pesan', 'Sukses menghapus');
            redirect('master_account','refresh');
        } else {
            $this->session->set_flashdata('pesan', 'Gagal menghapus');
            redirect('master_account','refresh');
        }
	}

	public function account_update()
	{
		if($this->input->post('edit')){
			if($this->user->update_account()){
			$this->session->set_flashdata('pesan', 'Sukses Update');
			redirect('master_account','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Update');
			redirect('master_account','refresh');	
		}
		}
	}

}

/* End of file MasterAccount.php */

?>