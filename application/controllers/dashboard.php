<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('admin');
		$this->load->model('m_product', 'product');
        // $this->load->library('../controllers/account');
    }
    

    public function index()
    {
        $data['dataProduct'] = $this->product->getProduct2();
		$data['konten']="home";
		$data['judul']="Welcome";
		$data['aktip1']="active";
		$data['aktip2']="";
		$data['aktip3']="";
		$data['aktip4']="";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
        $this->load->view('dashboard',$data);
	}
	
	public function product()
	{
		$data['konten']="product";
		$data['judul']="My Product";
		$data['aktip1']="";
		$data['aktip2']="active";
		$data['aktip3']="";
		$data['aktip4']="";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
		$this->load->view('dashboard', $data);	
	}
	
	public function shop()
	{
		$data['konten']="shop";
		$data['judul']="Shop";
		$data['aktip1']="";
		$data['aktip2']="";
		$data['aktip3']="";
		$data['aktip4']="";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
		$this->load->view('dashboard', $data);	
	}
	
	public function about()
	{
		$data['konten']="about";
		$data['judul']="About";
		$data['aktip1']="";
		$data['aktip2']="";
		$data['aktip3']="active";
		$data['aktip4']="";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
		$this->load->view('dashboard', $data);	
	}

	public function proses_login()
	{
		if($this->input->post('login')){
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_user');
				if($this->m_user->get_login()->num_rows()>0){
					$data=$this->m_user->get_login()->row();
					$array = array(
						'login' => TRUE,
						'full_name'=>$data->full_name,
						'email'=>$data->email,
						'address'=>$data->address,
						'phone'=>$data->phone,
						'username'=>$data->username,
						'password'=>$data->password,
						'account_id'=>$data->account_id
					);
					
					$this->session->set_userdata( $array );
					redirect('dashboard','refresh');
				} else {
					$this->session->set_flashdata('pesan', 'salah username dan password');
					redirect('dashboard/login','refresh');
				}
			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('dashboard/login','refresh');
			}
		}
	}

	public function proses_reset()
	{
		if($this->input->post('reset')){
			$this->form_validation->set_rules('resetUsername', 'Username', 'trim|required');
			$this->form_validation->set_rules('resetPassword', 'Password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_user');

				if($this->m_user->resetPW()==TRUE){
					$this->session->set_flashdata('pesan', 'Sukses Reset Password');
					redirect('dashboard/login','refresh');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal Reset Password');
					redirect('dashboard/login','refresh');
				}

			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('dashboard/register','refresh');
			}
		}
	}

	public function login()
	{
		$data['konten']="login";
		$data['judul']="Login";
		$data['aktip1']="";
		$data['aktip2']="";
		$data['aktip3']="";
		$data['aktip4']="active";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
		$this->load->view('dashboard',$data);
	}

	public function reset()
	{
		$data['konten']="resetPw";
		$data['judul']="Reset Password";
		$data['aktip1']="";
		$data['aktip2']="";
		$data['aktip3']="";
		$data['aktip4']="active";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
		$this->load->view('dashboard',$data);
	}

	public function register()
	{
		$data['judul']="Register";
		$data['konten']="register";
		$data['aktip1']="";
		$data['aktip2']="";
		$data['aktip3']="";
		$data['aktip4']="";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
		$this->load->view('dashboard',$data);
	}

	public function simpan()
	{
		if($this->input->post('submit')){
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model('m_user');

				if($this->m_user->masuk()==TRUE){
					$this->session->set_flashdata('pesan', 'Sukses Terdaftar');
					redirect('dashboard/login','refresh');
				} else {
					$this->session->set_flashdata('pesan', 'Gagal Terdaftar');
					redirect('dashboard/register','refresh');
				}

			} else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('dashboard/register','refresh');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('dashboard/login','refresh');
	}

}

/* End of file Controllername.php */

?>