<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('admin', 'admin');
		$this->load->model('m_product', 'product');
    }
    
    public function index(){
        // $this->deleteAccount();
        // $data['dataAccount'] = $this->admin->getListAccount();
        $data['dataProduct'] = $this->product->getListProduct();
		$data['konten']="product";
		$data['judul']="My Product";
		$data['aktip1']="";
		$data['aktip2']="active";
		$data['aktip3']="";
		$data['aktip4']="";
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
        $this->load->view('dashboard', $data);
    }
    
    public function detailProduct($product_id=''){
        $data['detailProduct'] = $this->product->getProductById($product_id);
		$data['konten']="detail_product";
		$data['judul']="My Product";
		$data['aktip1']="";
		$data['aktip2']="active";
		$data['aktip3']="";
        $data['aktip4']="";
        $product = $this->product->getProductById($product_id);
        $array = array(
            'product_id' => $product_id,
            'product_name' => $product->product_name,
            'image' => $product->image,
            'ammount' => $product->price,
            'stock' => $product->stock
        );
        $this->session->set_userdata( $array );
		if($this->session->userdata('login')==TRUE){
			$data['url']="logout";
			$data['log']=" Logout";
		} else {
			$data['url']="login";
			$data['log']=" Login";
		}
        $this->load->view('dashboard', $data);
    }

    /**
     * Function for Add Product
    */
    public function addProduct()
    {
        $product_name = "Puzzle Houes 1";
        $stock = 30;
        $price = 150000;
        $dimention = 25;
        $image = "product-3.jpg";
        
        $data = array (
                "product_id" => '',
                "product_name" => $product_name,
                "stock" => $stock,
                "price" => $price,
                "dimention" => $dimention,
                "image" => $image,
                "is_deleted" => 0   
            );
        $result =  $this->admin->addProduct($data);
        return $result;        
    }

    /**
     * Function for Update Product
    */
    public function updateProduct($id)
    {
        $product_id = $id;
        $product_name = "Puzzle Houes 1 Update";
        $stock = 30;
        $price = 150000;
        $dimention = 25;
        $image = "product-1.jpg";        
        $this->admin->updateProduct($product_id,$product_name,$stock,$price,$dimention,$image);                                    

    }
    
    /**
     * Function for Delete Product
    */
    public function deleteProduct()
    {                        
        $product_id = 3;
        $this->admin->deleteProduct($product_id);
    }

    
}
?>