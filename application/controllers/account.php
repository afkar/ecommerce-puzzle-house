<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('admin');
    }
    
    public function index(){
        $this->deleteAccount();
        $result = $this->admin->getListAccount();
        print_r($result);
    }

    /**
     * Function for Register
    */
    public function register()
    {
        $full_name = "Haha";
        $address = "Bang";
        $phone = "012607745674";
        $email = "haa@gmail.com";
        $username = "tes";
        $password = "tes123";
        $role_id = 1;

        $checkAccount = $this->admin->getAccount($username,$email);
        if(COUNT($checkAccount) == 0){
            $data = array (
                "account_id" => '',
                "full_name" => $full_name,
                "address" => $address,
                "phone" => $phone,
                "email" => $email,
                "username" => $username,
                "password" => $password,
                "role_id" => $role_id,
                "is_deleted" => 0   
            );
            $result =  $this->admin->register($data);
            return $result;
        } else {
            $result = array(
                "status_code" => 404,
                "account" => array(                            
                ),
                "message" => "Account Already Exist"
            );
        }
        

    }

    /**
     * Function for update Account
    */
    public function updateAccount()
    {
        $full_name = "AdamA";
        $address = "Malang";
        $phone = "085607745674";
        $email = "adam@gmail.com";
        $username = "adam";
        $password = "adam123";
        $role_id = 1;

        $this->admin->updateAccount(5,$full_name,$address,$phone,$email,$username,$password,$role_id);                            

    }

    /**
     * Function for Reset Password
    */
    public function resetPassword()
    {                
        $newPassword = "aaa";
        $account_id = 5;

        $checkAccount = $this->admin->getAccountById($account_id);        
        if(COUNT($checkAccount) != 0){            
            $this->admin->resetPassword(2,$newPassword);                            
        } else{            
            $result = array(
                "status_code" => 404,
                "account" => array(                                
                            ),
                "message" => "Your Account Not Found"
            );
        }

        

    }

    /**
     * Function for Delete Account
    */
    public function deleteAccount()
    {                        
        $account_id = 3;         
        $this->admin->deleteAccount($account_id);                                            
    }

    
}
?>