<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_Variable extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->model('admin');
    }
    
    public function index(){
        // $this->deleteAccount();
        // $result = $this->admin->getListAccount();
        // print_r($result);
    }

    /**
     * Function for Add Global Variable
    */
    public function addGlobalVariable()
    {
        $type = "Status";
        $value = 1;
        $description = "In Progress";        
        
        $data = array (
                "global_variable_id" => '',
                "type" => $type,
                "value" => $value,
                "description" => $description,                
                "is_deleted" => 0   
            );
        $result =  $this->admin->addGlobalVariable($data);
        return $result;        
    }

    /**
     * Function for Update Global Variable
    */
    public function updateGlobalVariable()
    {
        $global_varibale_id = 1;
        $type = "Puzzle Houes 1";
        $value = 30;
        $description = 150000;        
        $this->admin->updateGlobalVariable($global_varibale_id,$type,$value,$description);                                    

    }
    
    /**
     * Function for Delete Global Variable
    */
    public function deleteGlobalVariable()
    {                        
        $global_varibale_id = 1;         
        $this->admin->deleteGlobalVariable($global_varibale_id);                                            
    }

    
}
?>