<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('admin');
        date_default_timezone_set('Asia/Jakarta');
    }
    
    public function index(){
        $this->deleteOrder();
        $result = $this->admin->getListDetailOrder();
        print_r($result);
        exit();
    }

    /**
     * Function for Add Order
    */
    public function addOrder()
    {
        $account_id = 1;
        $product_id = 1;
        $payment_id = 1;
        $price = 150000;
        $output = 2;
        $amount = $price * $output;        
        $order_date = date('Y-m-d H:i:s');
        $status_id = 1;
        $username = "Afkar";
        $product_name = "Product 1";        
        $payment_name = "OVO";
        $address = "Tuban";
        
        $data = array (                
                "account_id" => $account_id,
                "product_id" => $product_id,
                "payment_id" => $payment_id,
                "amount" => $amount,
                "output" => $output,
                "order_date" => $order_date,
                "status_id" => $status_id,                
                "username" => $username,
                "product_name" => $product_name,
                "price" => $price,
                "payment_name" => $payment_name,
                "address" => $address                
            );
        $result =  $this->admin->addOrder($data);
        return $result;        
    }

    /**
     * Function for Update Status Order
    */
    public function updateStatusOrder()
    {
        $order_id = 2;
        $status_id = 2;
        $this->admin->updateStatusOrder($order_id,$status_id);                                    
    }
    
    /**
     * Function for Delete Order
    */
    public function deleteOrder()
    {                        
        $order_id = 2;         
        $this->admin->deleteOrder($order_id);                                            
    }

    
}
?>