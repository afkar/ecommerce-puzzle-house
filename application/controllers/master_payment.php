<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Payment extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_payment','payment');
    }
    

    public function index()
    {
        $data['dataPayment']=$this->payment->get_payment();
        $data['konten']='master_payment';
        $data['judul']='Data Payment';
        $this->load->view('admin_dashboard', $data);
    }

	public function tambah()
	{
		if($this->input->post('simpan')){
			if($this->payment->simpan_payment()){
				$this->session->set_flashdata('pesan', 'Sukses menambahkan');
				redirect('master_payment','refresh');
			} else {
				$this->session->set_flashdata('pesan', 'Gagal menambahkan');
				redirect('master_payment','refresh');
			}
		}
	}

    public function edit_payment($id)
    {
        $data=$this->payment->detail_payment($id);
        echo json_encode($data);
    }

	public function payment_update()
	{
		if($this->input->post('edit')){
			if($this->payment->update_payment()){
			$this->session->set_flashdata('pesan', 'Sukses Update');
			redirect('master_payment','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Hapus');
			redirect('master_payment','refresh');	
		}
		}
	}

	public function hapus($id)
	{
		if($this->payment->hapus_payment($id)){
            $this->session->set_flashdata('pesan', 'Sukses menghapus');
            redirect('master_payment','refresh');
        } else {
            $this->session->set_flashdata('pesan', 'Gagal menghapus');
            redirect('master_payment','refresh');
        }
	}

}

/* End of file Master_Payment.php */

?>