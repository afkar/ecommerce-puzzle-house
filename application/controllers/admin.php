<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Admin extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('m_user', 'user');
            
            
            if($this->session->userdata('loginAdmin')!=true){
                redirect(base_url('index.php/login'),'refresh');
            }
        }
        
        public function index()
        {
            $da=$this->user->get_role()->row();
            $array = array(
                'role_idAdmin' => $da->role_id,
                'role_nameAdmin' => $da->role_name
            );
            $this->session->set_userdata( $array );
            
            $data['konten']='home_admin';
            $data['judul']='Admin';
            $this->load->view('admin_dashboard', $data);
        }

        public function logout()
        {
            
            $this->session->sess_destroy();
            
            redirect('login','refresh');
            
        }
    
    }
    
    /* End of file Admin.php */
    
?>