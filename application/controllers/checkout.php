<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Checkout extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            //Do your magic here
            $this->load->model('m_product', 'product');
            $this->load->model('m_user', 'account');
            $this->load->model('m_confirmBuy', 'confirm');
            $this->load->model('m_payment','payment');
            date_default_timezone_set('Asia/Jakarta');
            
        }
        
    
        public function confirmBuy()
        {
            $p=$this->input->post('payment');
            if($this->confirm->prosesOrder($p)==TRUE){
                $this->session->set_flashdata('pesan', 'Sukses Membeli');
                redirect('checkout/payment','refresh');
            } else {
                $this->session->set_flashdata('pesan', 'Gagal Membeli');
                redirect('product','refresh');
            }
        }
	
        public function pay()
        {
            $data['dataProduct'] = $this->product->getListProduct();
            $data['konten']="product";
            $data['judul']="My Product";
            $data['aktip1']="";
            $data['aktip2']="active";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE){
                $data['url']="logout";
                $data['log']=" Logout";
            } else {
                $data['url']="login";
                $data['log']=" Login";
            }
            $this->session->set_flashdata('pesan', 'Sukses Membeli');
            $this->load->view('dashboard', $data);	
        }

        public function payment()
        {
            $data['dataOrder']=$this->confirm->get_order();
            $data['konten']="payment";
            $data['judul']="Payment";
            $data['aktip1']="";
            $data['aktip2']="";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE){
                $data['url']="logout";
                $data['log']=" Logout";
            } else {
                $data['url']="login";
                $data['log']=" Login";
            }
            $this->load->view('dashboard', $data);	
        }

        public function buy()
        {
            $data['dataPayment']=$this->payment->get_payment();
            $da = array(
                'qty' => $this->input->post('qty')
            );
            $this->session->set_userdata( $da );
            $data['dataAmount']=$this->session->userdata('ammount') * $this->input->post('qty');
            $data['aktip1']="";
            $data['aktip2']="active";
            $data['aktip3']="";
            $data['aktip4']="";
            if($this->session->userdata('login')==TRUE && $this->session->userdata('stock') >= $this->input->post('qty')){
                $data['url']="logout";
                $data['log']=" Logout";
                $data['konten']="check_out";
                $data['judul']="Checkout";
            } elseif($this->session->userdata('login')==TRUE && $this->session->userdata('stock') <= $this->input->post('qty')){
                $data['url']="logout";
                $data['log']=" Logout";
                $data['dataProduct'] = $this->product->getListProduct();
                $data['konten']="product";
                $data['judul']="My Product";
                $this->session->set_flashdata('pesan', 'Stock tidak mencukupi pembelian');
            } else {
                $data['url']="login";
                $data['log']=" Login";
                $data['konten']="login";
                $data['judul']="Login";
                $this->session->set_flashdata('pesan', 'Silahkan Login Terlebih Dahulu');
            }
            $this->load->view('dashboard', $data);
        }
    
    }
    
    /* End of file Checkout.php */
        
?>