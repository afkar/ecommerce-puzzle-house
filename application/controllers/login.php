<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    public function index()
    {
        $this->load->view('admin');
    }

    public function proses_login()
    {
        if($this->input->post('loginAdmin')){
            $this->form_validation->set_rules('usernameAdmin', 'Username', 'trim|required');
            $this->form_validation->set_rules('passwordAdmin', 'Password', 'trim|required');
            if ($this->form_validation->run() == TRUE) {
                $this->load->model('m_user');
                if($this->m_user->get_loginAdmin()->num_rows()>0){
                    $data=$this->m_user->get_loginAdmin()->row();
                    $array = array(
                        'loginAdmin' => TRUE,
                        'full_nameAdmin'=>$data->full_name,
                        'emailAdmin'=>$data->email,
                        'addressAdmin'=>$data->address,
                        'phoneAdmin'=>$data->phone,
                        'usernameAdmin'=>$data->username,
                        'passwordAdmin'=>$data->password,
                        'account_idAdmin'=>$data->account_id,
                        'role_idAdmin'=>$data->role_id
                    );
                    
                    $this->session->set_userdata( $array );
                    redirect('admin','refresh');
                } else {
                    $this->session->set_flashdata('pesan', 'Username dan Password Salah');
                    redirect('login','refresh');
                }
            } else {
                $this->session->set_flashdata('pesan', validation_errors());
                redirect('login','refresh');
            }
        }
    }

}
?>