<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Order extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_order','order');
    }

    public function index()
    {
        $data['dataOrder']=$this->order->get_order();
        $data['status']=$this->order->get_status();
        $data['konten']='master_order';
        $data['judul']='Data Order';
        $this->load->view('admin_dashboard', $data);
    }

    public function edit_order($id)
    {
        $data=$this->order->detail_order($id);
        echo json_encode($data);
    }

	public function order_update()
	{
		if($this->input->post('edit')){
			if($this->order->update_order()){
			$this->session->set_flashdata('pesan', 'Sukses Update');
			redirect('master_order','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'Gagal Hapus');
			redirect('master_order','refresh');	
		}
		}
	}

	public function hapus($id)
	{
		if($this->order->hapus_order($id)){
            $this->session->set_flashdata('pesan', 'Sukses menghapus');
            redirect('master_order','refresh');
        } else {
            $this->session->set_flashdata('pesan', 'Gagal menghapus');
            redirect('master_order','refresh');
        }
	}

}

/* End of file Master_Order.php */

?>